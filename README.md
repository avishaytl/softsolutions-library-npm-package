# npm package
## [Softsolutions LTD](https://www.softsolutions.co.il) Library For React/React Native


**softsolutions-library** package is a public library for *Softsolutions LTD*.


## Installation

Use the package manager [npm](https://www.npmjs.com) to install.

```bash
npm install softsolutions-library
```

Or the package manager [yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable) to install

```bash
yarn add softsolutions-library
```

## Usage


```js
import soft from 'softsolutions-library'; // returns Main lab
import softReact from 'softsolutions-library/react.js'; // returns React lab
import softReactNative from 'softsolutions-library/react-native.js'; // returns React Native lab

soft.connectingMessage(); // returns connection console Main 
softReact.connectingReactMessage(); // returns connection console React 
softReactNative.connectingReactNativeMessage(); // returns console connection React Native  
```


## Main


usage Main functions


```js 
import soft from 'softsolutions-library';

const { connectingMessage } = soft;

connectingMessage();
```


## React

usage React functions


```js 
import softReact from 'softsolutions-library/react.js';

const { connectingReactMessage } = softReact;

connectingReactMessage(); 
```


## React Native

usage React Native functions


```js 
import softReactNative from 'softsolutions-library/react-native.js';

const { connectingReactNativeMessage } = softReactNative;

connectingReactNativeMessage();
```


#### Support 
avishay@softsolutions.co.il